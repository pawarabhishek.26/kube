#!/bin/sh

for ARGUMENT in "$@"
do

    KEY=$(echo $ARGUMENT | cut -f1 -d=)
    VALUE=$(echo $ARGUMENT | cut -f2 -d=)

    case "$KEY" in
            APP_NAME)                   APP_NAME=${VALUE} ;;
            CHART_NAME)                 CHART_NAME=${VALUE} ;;
            ENVIRONMENT)                ENVIRONMENT=${VALUE} ;;
            IMAGE_TAG)                  IMAGE_TAG=${VALUE} ;;
            KUBE_ENVIRONMENT)           KUBE_ENVIRONMENT=${VALUE} ;;
            *)
    esac


done

echo "APP_NAME = $APP_NAME"
echo "CHART_NAME = $CHART_NAME"
echo "ENVIRONMENT = $ENVIRONMENT"

echo Chart Name : "${CHART_NAME}", Environment : "${ENVIRONMENT}"


export environment=$KUBE_ENVIRONMENT AWS_PROFILE=$KUBE_ENVIRONMENT

#cd /opt/vkube-infra
#
#git checkout vinfra
#git pull origin vinfra
#
#
#source envs/addsale/variables.sh
#
#
#cd /opt/vcharts
#
#git pull origin master
#
#helm upgrade --install $APP_NAME $CHART_NAME --namespace $ENVIRONMENT -f $CHART_NAME/env/values.yaml -f $CHART_NAME/env/$ENVIRONMENT/values.yaml
#

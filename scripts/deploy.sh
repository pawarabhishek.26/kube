#!/bin/bash

APP_NAME=$1
CHART_NAME=$2
ENVIRONMENT=$3

echo Chart Name : "${CHART_NAME}", Environment : "${ENVIRONMENT}"

cd /opt/fresco

git checkout vinfra
git pull origin vinfra

export environment=addsale ENVIRONMENT=addsale AWS_PROFILE=addsale

source envs/addsale/variables.sh


cd /opt/vcharts

git pull origin master

helm upgrade --install $APP_NAME $CHART_NAME --namespace $ENVIRONMENT -f $CHART_NAME/env/values.yaml -f $CHART_NAME/env/$ENVIRONMENT/values.yaml


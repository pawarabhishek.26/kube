# FROM openjdk:8-jdk-alpine
FROM adoptopenjdk/openjdk11:alpine
RUN apk update
# RUN apk --no-cache add openjdk11 --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community
# RUN export JAVA_HOME=/usr/lib/jvm/java-11-openjdk/bin
# RUN export PATH=${PATH}:${JAVA_HOME}

# RUN apk --no-cache add openjdk11 --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community
# RUN echo $PATH
# ENV PATH=${PATH}:/usr/lib/jvm/java-11-openjdk/bin
# RUN echo $PATH

RUN apk --no-cache add curl wget unzip git maven jq vim ca-certificates curl openrc python3

# RUN apk add docker
# RUN apk update
# RUN rc-update add docker boot
# RUN service docker start

RUN apk add --no-cache bash
RUN apk add --no-cache openssl

RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
RUN chmod 700 get_helm.sh
RUN ./get_helm.sh

RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.14.6/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin/kubectl

# AWS CLI
RUN pip3 install awscli --upgrade
RUN echo $PATH
ENV PATH=$HOME/.local/bin:${PATH}
RUN echo $PATH
RUN mkdir -p /bin/aws/
# RUN pip3 install awscli --upgrade
# RUN export PATH=$HOME/.local/bin:$PATH
# RUN mkdir -p /bin/aws/

# Get ENV
ARG GITLAB_OAUTH2_TOKEN
ENV GITLAB_OAUTH2_TOKEN=$GITLAB_OAUTH2_TOKEN
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

# check if it actually works
RUN mvn -v
RUN kubectl version --client
RUN helm version
RUN aws --version
# RUN docker -v /var/run/docker.sock:/var/run/docker.sock version
WORKDIR /opt/
RUN git clone https://oauth2:$GITLAB_OAUTH2_TOKEN@gitlab.com/fynd/vision/content/vkube-infra.git /opt/vkube-infra
RUN git clone https://oauth2:$GITLAB_OAUTH2_TOKEN@gitlab.com/fynd/vision/content/vcharts.git /opt/vcharts

ADD scripts/* /opt/
COPY config/maven/settings.xml /usr/share/java/maven-3/conf/settings.xml  

CMD ["/bin/sh"]
